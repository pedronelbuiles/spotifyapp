import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class SpotifyService {

  constructor( private http:HttpClient) { }

  getNewReleases(){
    const headers = new HttpHeaders({
    'Authorization': 'Bearer BQBpUH09cvRs21tUWYgfohXPKLaYQBvsESIj1nfwq77YOvZiowBUvarezK1bS5C1SoSZV0Rs51xF2S4LPNMfKV2fVARdOaIPk0j9oM8CUFWpAcLXwgWqC9o3Gedd66sMFgzOrQtk2n15lhyh8WfI7OveeXXp6rxXMQ'
    });
    return this.http.get('	https://api.spotify.com/v1/browse/new-releases',{ headers })
                .pipe(map(data => data['albums'].items ));
  }

  getArtista( termino:string){
    const headers = new HttpHeaders({
      'Authorization': 'Bearer BQBpUH09cvRs21tUWYgfohXPKLaYQBvsESIj1nfwq77YOvZiowBUvarezK1bS5C1SoSZV0Rs51xF2S4LPNMfKV2fVARdOaIPk0j9oM8CUFWpAcLXwgWqC9o3Gedd66sMFgzOrQtk2n15lhyh8WfI7OveeXXp6rxXMQ'
      });
      return this.http.get(`https://api.spotify.com/v1/search?query=${ termino }&type=artist&market=CO&offset=0&limit=20`,{ headers })
              .pipe(map(data => data['artists'].items ));
  }
}
